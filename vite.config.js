import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import react from '@vitejs/plugin-react'
import sassPlugin from 'vite-plugin-sass'; 
import windiCSS from 'vite-plugin-windicss';


export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/js/app.jsx'],
            refresh: true,
        }),
        react(),
        sassPlugin(),
        windiCSS(),
    ], 
});

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except'=> ['login','register','logout']]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
    
        $credentials = $request->only('email', 'password');
    
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Non autorisé'
            ], 401);
        }
    
        $user = Auth::user();
        $token = $user->createToken('Token Name')->plainTextToken;
    
        return response()->json([
            'status' => 'success',
            'user' => $user,
            'authorization' => [
                'token' => $token,
                'type' => 'bearer'
            ]
        ]);
    }

    public function register(Request $request){
        $request->validate([
            'name'=> 'required|string',
            'email'=>'required|string|email|unique:users',
            'password'=>'required|string'
        ]);

        $user= User::create([
            'name'=> $request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
        ]);

        return response()->json([
            'status'=> 'success',
            'message'=>'Utilisateur créé avec succès'
        ]);

    }

    public function logout(){
        Auth::logout();
        return response()->json([
            'status'=>'success',
            'message'=>'Déconnexion reussi'
        ]);
    }

    public function refresh(){
        return response()->json([
            'status' => 'success',
            'user' => Auth::user(),
            'authorisation' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }
}

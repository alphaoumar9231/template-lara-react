<?php

namespace App\Http\Controllers;

use App\Models\PostController;
use Illuminate\Http\Request;

class PostControllerController extends Controller
{
    public function index(){
        $post = PostController::all();
        return $post;
    }

    public function store (Request $request){
        $input = $request->validate([
            'nom'=>'required|string',
            'description'=>'required|string'
        ]);

        PostController::Create($input);

        return response()->json([
            'status'=>'success',
            'message'=>'Le post a été ajouter avec succée !'
        ]);

    }

    public function show($id)
    {

        $contact = PostController::findOrFail($id);
        return response()->json([
            'status'=>'success',
            'data'=>$contact
        ]);

    }

    public function update(Request $request, $id){
        $input = $request->validate([
            'nom'=>'required|string',
            'description'=>'required|string'
        ]);

        PostController::whereId($id)->update($input);

        return response()->json([
            'status' => 'success',
            'message'=> 'Mis à jour reussi !'
        ]);
    }

    public function destroy ($id){
        PostController::whereId($id)->delete();

        return response()->json([
            'status'=>'success',
            'message'=>'Suppression reussi !'
        ]);
    }
}

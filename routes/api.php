<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostControllerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::post('/login', 'AuthController@login')->name('login');
// Route::post('/register', 'AuthController@register')->name('register');
// Route::post('/logout', 'AuthController@logout')->name('logout');
// Route::post('/refresh', 'AuthController@refresh')->name('refresh');

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');

});

Route::group(['midleware'=>['auth.jwt']], function (){
    Route::get('post', [PostControllerController::class, 'index']);
    Route::post('post', [PostControllerController::class, 'store']);
    Route::put('post/update/{id}', [PostControllerController::class, 'update']);
    Route::get('post/{id}', [PostControllerController::class, 'show']);
    Route::delete('post/{id}', [PostControllerController::class, 'destroy']);
});


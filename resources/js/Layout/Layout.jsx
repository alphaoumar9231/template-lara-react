import React from 'react'
import { Outlet } from 'react-router-dom'
import SideBar from '../Component/SideBar/SideBar'
import NavBar from '../Component/NavBar/NavBar'

export default function Layout() {
	return (
		<div id='body'>
			<NavBar id='navbar' />
			<SideBar id='sidebar' />
			<div className="p-4 sm:ml-64 mt-14">
				<Outlet id='Outlet' />
			</div>
		</div>
	)
}

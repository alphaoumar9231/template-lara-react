import React from 'react'
import { Navigate, Route, Routes, useNavigate } from 'react-router-dom'
import Login from './Login/Login'
import Register from './Register/Register'
import Layout from '../Layout/Layout'
import Dashboard from './Dashboard/Dashboard'
import Show from './Show/Show'
import 'tailwindcss/tailwind.css'
export default function Index() {

	const isConnected = JSON.parse(sessionStorage.getItem('token'))

	const ProtectedRoute = ({ isConnected }) => {
        return isConnected===null || isConnected === undefined || isConnected === ''? (
            <Navigate to="/" replace={true} />
        ) : (
            <Layout />
        );
    };


	return (
		<Routes>
			<Route path='/' element={<Login />} />
			<Route path='/register' element={<Register />} />
			<Route path='/admin' element={<ProtectedRoute isConnected={isConnected} />}>
				<Route path='/admin/dashboard' element={<Dashboard />} />
				<Route path='/admin/show' element={<Show />} />
			</Route>
		</Routes>
	)
}

import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

export default function Register() {
	const [email, setEmail] = useState('');
	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const navigate = useNavigate();

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
	};

	const handlePasswordChange = (e) => {
		setPassword(e.target.value);
	};

	const handleNameChange = (e) => {
		setName(e.target.value);
	};

	const handleRegistration = (e) => {
		e.preventDefault();

		const user = {
			name: name,
			email: email,
			password: password
		};

		axios.post('/api/register', user)
			.then(() => {
				navigate('/')
				setName(' ');
				setEmail(' ');
				setPassword(' ');
				toast.success('Incription réussie !');
			})
			.catch((error) => {
				console.log(error.response.data);
			});

	};
	return (
		<div className='bg-gray-400 min-h-screen'>
			<div className="flex min-h-full flex-col justify-center px-6 py-12 lg:px-8">
				<div className="sm:mx-auto sm:w-full sm:max-w-sm">
					<img className="mx-auto h-10 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="Your Company" />
					<h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">Connexion</h2>
				</div>

				<div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
					<form className="space-y-6" onSubmit={handleRegistration}>
						<div>
							<label for="nom" className="block text-sm font-medium leading-6 text-gray-900">Nom</label>
							<div className="mt-2">
								<input id="nom" value={name}
									onChange={handleNameChange}
									name="email" type="email" autocomplete="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
							</div>
						</div>
						<div>
							<label for="email" className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
							<div className="mt-2">
								<input id="email" value={email}
									onChange={handleEmailChange}
									name="email" type="email" autocomplete="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
							</div>
						</div>

						<div>
							<div className="flex items-center justify-between">
								<label for="password" className="block text-sm font-medium leading-6 text-gray-900">Password</label>

							</div>
							<div className="mt-2">
								<input id="password" value={password}
									onChange={handlePasswordChange}
									name="password" type="password" autocomplete="current-password" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
							</div>
						</div>

						<div>
							<button type="submit" className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Sign in</button>
						</div>
					</form>

					<p className="mt-10 text-center text-sm text-gray-500">
						vous êtes déjà inscrit
						<Link to="/" className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500"> Connectez-vous !</Link>
					</p>
				</div>
			</div>
		</div>
		// <form onSubmit={handleRegistration}>
		// 	<h2>Inscription</h2>
		// 	<div>
		// 		<label htmlFor="name">Nom</label>
		// 		<input
		// 			type="name"
		// 			id="name"
		// 			value={name}
		// 			onChange={handleNameChange}
		// 		/>
		// 	</div>
		// 	<div>
		// 		<label htmlFor="email">Email:</label>
		// 		<input
		// 			type="email"
		// 			id="email"
		// 			value={email}
		// 			onChange={handleEmailChange}
		// 		/>
		// 	</div>
		// 	<div>
		// 		<label htmlFor="password">Mot de passe:</label>
		// 		<input
		// 			type="password"
		// 			id="password"
		// 			value={password}
		// 			onChange={handlePasswordChange}
		// 		/>
		// 	</div>
		// 	<button type="submit">S'inscrire</button>
		// 	<Link to="/">Connectez-vous !</Link>

		// </form>
	)
}

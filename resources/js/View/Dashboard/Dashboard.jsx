import React, { useEffect, useState } from 'react'
import './Dashboard.css'
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';


export default function Dashboard() {

	// CRUD post
	const [description, setdescription] = useState('');
	const [nom, setnom] = useState('');
	const [data, setData] = useState();

	const handledescriptionChange = (e) => {
		setdescription(e.target.value);
	};
	const handlenomChange = (e) => {
		setnom(e.target.value);
	};

	const tokenAuth = JSON.parse(window.sessionStorage.getItem('token'));
	const token = tokenAuth?.token ?? '';
	const requestConfig = {
		headers: {
			Accept: "application/json",
			Authorization: `Bearer ${token}`,
		},
	};

	const handleRegistration = (e) => {
		e.preventDefault();
		const post = {
			nom: nom,
			description: description,
		};
		axios.post('/api/post', post)
			.then(({ data }) => {
				fetchData()
				setnom('');
				setdescription('');
				toast.success(data.status);
			})
			.catch((error) => {
				console.log(error.response.data);
			});


	};


	const fetchData = () => {
		axios.get('/api/post')
			.then(({ data }) => {
				setData(data);

			})
			.catch((error) => {
				console.log(error.response.data);
			});
	}

	useEffect(() => {
		fetchData()
	}, [])

	const deletePost = async (id) => {
		const isConfirm = await Swal.fire({
			title: 'Etes-vous sûr(e) ?',
			text: "Vous ne pourrez pas revenir en arrière!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: 'Annuler',
			confirmButtonText: 'Oui, supprimez-le!'
		}).then((result) => {
			return result.isConfirmed
		});

		if (!isConfirm) {
			return;
		}

		await axios.delete(`/api/post/${id}`).then(({ data }) => {
			toast.warning(data.message);

			fetchData()
		}).catch(({ response: { data } }) => {
			toast.error(data.message);

		})

	}


	return (
		<div className='dashboard '>
			<div>

				<div>


					<div >
						<div className='flex justify-end p-3'>

							<button data-modal-target="defaultModal" data-modal-toggle="defaultModal" className="block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" type="button">
								Ajouter un poste
							</button>

							<div id="defaultModal" tabindex="-1" aria-hidden="true" className="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
								<form onSubmit={handleRegistration} className='border-2 border-blue-700  rounded-lg dark:border-gray-700 relative w-full max-w-4xl max-h-full'>

									<div
										style={{
											fontFamily:
												'-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
											boxSizing: 'border-box',
											background: '#1262A8',
											padding: '25px 0',
											textAlign: 'center'
										}}

										className='flex items-start justify-between px-5'
									>
										<p
											style={{
												boxSizing: 'border-box',
												fontFamily:
													'Arial, "Helvetica Neue", Helvetica, sans-serif',
												fontSize: '16px',
												fontWeight: 'bold',
												color: '#FFF',
												textDecoration: 'none'
											}}

											className='text-uppercase'
										>
											Ajouter un poste
										</p>
										<button type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="defaultModal">
											<svg className="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
												<path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
											</svg>
											<span className="sr-only">Close modal</span>
										</button>
									</div>
									<div className='p-4 bg-white'>


										<div className="relative mb-3">
											<input
												value={nom}
												onChange={handlenomChange}
												type="text" id="floating_filled"
												className="block rounded-t-lg px-2.5 pb-2.5 pt-5 w-full text-sm text-gray-900 bg-gray-50 dark:bg-gray-700 border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer font-bold" placeholder=" " />
											<label for="floating_filled"
												className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-4 z-10 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4 font-bold">Nom</label>
										</div>
										<div className="relative mb-3">
											<textarea
												value={description}
												onChange={handledescriptionChange}
												id="message" rows="4"
												className="block rounded-t-lg px-2.5 pb-2.5 pt-5 w-full text-sm text-gray-900 bg-gray-50 dark:bg-gray-700 border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer font-bold"
											></textarea>
											<label for="message" className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-4 z-10 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4 font-bold">Description</label>
										</div>
										<div className='flex justify-center'>
											<button type="submit"
												style={{
													boxSizing: 'border-box',
													boxShadow:
														'0 2px 3px rgba(0, 0, 0, 0.16)',
													fontFamily:
														'Arial, "Helvetica Neue", Helvetica, sans-serif',
													display: 'inline-block',
													width: '200px',
													minHeight: '20px',
													padding: '10px',
													backgroundColor: '#1262A8',
													borderRadius: '3px',
													color: '#ffffff',
													fontSize: '18px',
													lineHeight: '25px',
													textAlign: 'center',
													textDecoration: 'none',
													WebkitTextSizeAdjust: 'none'
												}}
												data-modal-hide="defaultModal"
												className="mb-3  text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 font-bold">Enregistrer</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div className="relative overflow-x-auto">
							<table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
								<thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
									<tr>
										<th scope="col" className="px-6 py-3">
											Nom
										</th>
										<th scope="col" className="px-6 py-3">
											Description
										</th>
										<th scope="col" className="px-6 py-3">
											Action
										</th>

									</tr>
								</thead>
								<tbody>
									{
										data?.sort((a, b) => b.id - a.id).map((item, index) => {
											return <tr key={index} className='bg-white border-b dark:bg-gray-800 dark:border-gray-700"'>
												<td scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">{item.nom}</td>
												<td className="px-6 py-4" >{item.description}</td>
												<td className="px-6 py-4" >
													<div className='flex gap-3'>
														<Link
															to={{
																pathname: '/admin/show',
																state: { id: item.id }
															}}
															className='btn btn-success'
														>
															Afficher
														</Link>

														<button className='btn btn-info'>
															Modifier
														</button>

														<button className='btn btn-danger' onClick={()=>deletePost(item.id)}>
															Supprimer
														</button>
													</div>
												</td>
											</tr>
										})
									}
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	)
}

{/* <div className='conte'>
	<table className='mytable'>
		<tr>
			<td className='td1' >
				<table width="100%" cellpadding="0" cellspacing="0" style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box;">
					<tr>
						<td style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; background: #1262A8; padding: 25px 0; text-align: center;">
							<a style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; font-weight: bold; color: #F8A434; text-decoration: none;" href="#">

								VOLKENO RH
							</a>
						</td>
					</tr>

					<tr>
						<td style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;" width="100%">
							<table style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; width: auto; max-width: 570px; margin: 0 auto; padding: 0;" align="center" width="570" cellpadding="0" cellspacing="0">
								<tr>
									<td style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px;">
										<h1 style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;">
											Bonjour Quynn Gallegos
										</h1>

										<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
											Un compte ADMIN vous a été créé pour accéder à la plateforme VOLKENO RH.
										</p>
										<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
											Email: luzonymo@mailinator.com <br> Mot de passe: Yza-#Zk-h3t-3
										</p>
										<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
											Pour des raisons de sécurité, nous vous conseillons fortement de supprimer ce message après lecture.
										</p>

										<table style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; width: 100%; margin: 30px auto; padding: 0; text-align: center;" align="center" width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td align="center" style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box;">

													<a href="http://127.0.0.1:8000" style="box-sizing: border-box; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; display: inline-block; width: 200px; min-height: 20px; padding: 10px; background-color: #1262A8; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px; text-align: center; text-decoration: none; -webkit-text-size-adjust: none;" className="button" target="_blank">
														Connectez-vous
													</a>
												</td>
											</tr>
										</table>

										<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
											Nous vous souhaitons une belle expérience sur VOLKENO RH!
										</p>

										<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">


											Cordialement
										</p>

										<table style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;">
											<tr>
												<td style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
													<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">

														Si vous n'arrivez pas à cliquer sur le bouton
													</p>

													<p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; text-align: left; margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">
														<a style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #1262A8;" href="http://127.0.0.1:8000" target="_blank">
															http://127.0.0.1:8000
														</a>
													</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div> */}
import axios from 'axios';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import './Home.css'
import Sidebar from '../../Component/SideBar/SideBar';

export default function Home() {

	const user = JSON.parse(sessionStorage.getItem('user'))
	const navigate = useNavigate();

	const handleLogout = () => {
		axios.post('/api/logout')
			.then(({ data }) => {
				sessionStorage.removeItem('token');
				sessionStorage.removeItem('user');
				navigate('/')
				alert(data.status);
			})
	}

	// CRUD post
	const [description, setdescription] = useState('');
	const [nom, setnom] = useState('');

	const handledescriptionChange = (e) => {
		setdescription(e.target.value);
	};



	const handlenomChange = (e) => {
		setnom(e.target.value);
	};

	const tokenAuth = JSON.parse(window.sessionStorage.getItem('token'));
	const token = tokenAuth?.token ?? '';


	const requestConfig = {
		headers: {
			Accept: "application/json",
			Authorization: `Bearer ${token}`,
		},
	};

	const handleRegistration = (e) => {
		e.preventDefault();

		const post = {
			nom: nom,
			description: description,
		};

		axios.post('/api/post', post)
			.then(({ data }) => {
				alert(data.status);
			})
			.catch((error) => {
				console.log(error.response.data);
			});

		setnom('');
		setdescription('');
	};


	return (
		<div>
			<Sidebar />
		</div>






	)
}



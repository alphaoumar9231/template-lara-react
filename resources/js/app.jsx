import './bootstrap';
import { BrowserRouter } from 'react-router-dom';
import { createRoot } from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import React from 'react';
import Index from './View/Index'
import '../css/app.css';
import 'flowbite';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'sweetalert2/dist/sweetalert2.css';



createRoot(document.getElementById('app')).render(
	<BrowserRouter>
	  <Index />
	  <ToastContainer />
	</BrowserRouter>
  );